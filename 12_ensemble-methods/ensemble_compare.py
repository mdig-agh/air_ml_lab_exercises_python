import numpy as np
import matplotlib.pyplot as plt

from sklearn import datasets

from sklearn.ensemble import BaggingClassifier, AdaBoostClassifier, GradientBoostingClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import StratifiedKFold
from sklearn.model_selection import cross_val_score

RANDOM_STATE = 1

np.set_printoptions(precision=2)

#%% Load the dataset.

X, y = None  # FIXME: implement

#%% Define classifiers and classifier ensembles.

# FIXME: implement

#%% Compare performance of the models.

# FIXME: implement

#%% Plot OOB estimates for Gradient Boosting Classifier.

# FIXME: implement
